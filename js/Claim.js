function Claim(){

    this.date = get_date_string(); 
    this.peoplecount = "0";
	this.names = "";
	this.speed = "30";
	this.weather ="0";
	this.visibility = "0";
	this.happened = "0";
	this.details = "";
	this.witnesses = "";
	this.damagedone = "";
	this.damage = "";
	this.othername = "";
	this.othermobile = "";
	this.othertele = "";
	this.otherinsure = "";
	this.othercar = "";
	this.name = "";
	this.policy = "";
	this.email = "";
	this.mobile = "";
	this.tele = "";
	this.car = "";
	this.location = "";
	this.image = "";
	this.id = -1; // unsaved is -1
	this.submitdate = "";
	this.type = "NEW"; // NEW, DRAFT or PREV
	this.dirty = true; // TODO - should only be dirty if any value is different from new claim

    // Store application data information into the localStorage.
    this.save = function(){
        // Create a claim data object.
        var claimData = {
            date : this.date,
            peoplecount: this.peoplecount,
			names: this.names,
			speed: this.speed,
			weather: this.weather,
			visibility: this.visibility,
			happened: this.happened,
			details: this.details,
			witnesses: this.witnesses,
			damagedone: this.damagedone,
			damage: this.damage,
			othername: this.othername,
			othermobile: this.othermobile,
			othertele: this.othertele,
			otherinsure: this.otherinsure,
			othercar: this.othercar,
			name: this.name,
			policy: this.policy,
			email: this.email,
			mobile: this.mobile,
			tele: this.tele,
			car: this.car,
			location: this.location,
			image: this.image,
			submitdate: this.submitdate,
			id: this.id,
			type: this.type
        };
        // Store dataobject into the local storage.
        var store = new Store();
		if(this.id == -1 && this.type != "NEW"){ // if type isn't new anymore, assign id
			// get new id and store
			var sequence = store.get('SEQUENCE');
			if(sequence == null)
				store.put('SEQUENCE', 0);
			claimData.id = parseInt(store.get('SEQUENCE')) + 1;
			store.put('SEQUENCE', claimData.id);
		}
        store.put('CLAIM_'+claimData.id, JSON.stringify(claimData));
    }
	
    // Retrieve application information from the localStorage. ONLY ONE CLAIM FOR NOW!
    this.retrieve = function(id){
        var store = new Store();
		if(store.get('CLAIM_'+id) != null){
			var claimData = JSON.parse(store.get('CLAIM_'+id));
			// Restore claim data detail from the storage.
			if(claimData != null){
				this.date = claimData.date;
				this.peoplecount = claimData.peoplecount;
				this.names = claimData.names;
				this.speed = claimData.speed;
				this.weather = claimData.weather;
				this.visibility = claimData.visibility;
				this.happened = claimData.happened;
				this.details = claimData.details;
				this.witnesses = claimData.witnesses;
				this.damagedone = claimData.damagedone;
				this.damage = claimData.damage;
				this.othername = claimData.othername;
				this.othermobile = claimData.othermobile;
				this.othertele = claimData.othertele;
				this.otherinsure = claimData.otherinsure;
				this.othercar = claimData.othercar;
				this.name = claimData.name;
				this.policy = claimData.policy;
				this.email = claimData.email;
				this.mobile = claimData.mobile;
				this.tele = claimData.tele;
				this.car = claimData.car;
				this.location = claimData.location;
				this.image = claimData.image;
				this.submitdate = claimData.submitdate;
				this.id = claimData.id;
				this.type = claimData.type;
			}
		}
    }

    // function will clear all the claim details from the storage.
    this.clean = function(){
        localStorage.clear();
    }
	
	function get_date_string(){
		var date = new Date();
		//zero-pad a single zero if needed
		var zp = function (val){
			return (val <= 9 ? '0' + val : '' + val);
		}

		//zero-pad up to two zeroes if needed
		var zp2 = function(val){
			return val <= 99? (val <=9? '00' + val : '0' + val) : ('' + val ) ;
		}

		var d = date.getDate();
		var m = date.getMonth() + 1;
		var y = date.getFullYear();
		var h = date.getHours();
		var min = date.getMinutes();
		var s = date.getSeconds();
		var ms = date.getMilliseconds();
		return '' + y + '-' + zp(m) + '-' + zp(d);
	}
}
