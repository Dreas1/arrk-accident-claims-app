var MAX_LOCATIONS = 5; // location history
var claim; // current claim being viewed
var store = new Store();
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
        open("claimhome.html", "_self", "", true);
    }
};

function validateLogin() {
    open("claimhome.html", "_self", "", true);
}

function validateRegister() {
    open("login.html", "_self", "", true);
}

function resendPassword() {
    open("login.html", "_self", "", true);
}

function yourDetails() {
    open("profile.html", "_self", "", true);
}

function newClaim() {
	// create and store new blank claim
	store.put('CURRENT_CLAIM_ID', -1);
	claim = new Claim();
	claim.save();
    open("newclaim_page1.html", "_self", "", true);
}

function draftClaims() {
    open("draft_claims.html", "_self", "", true);
}

function prevClaims() {
    open("prev_claims.html", "_self", "", true);
}

function captureLocation() {
    navigator.geolocation.getCurrentPosition(onSuccessLocation, onErrorLocation);
}

function onSuccessLocation(position) {
    console.log('Location success: ' + position.coords.latitude + ',' + position.coords.longitude);
    alert('Location saved: ' + position.coords.latitude + ',' + position.coords.longitude);
    var locations = []

    // store new location in local storage
    if (window.localStorage.getItem('LOCATION_LIST') != null)
        locations = JSON.parse(window.localStorage.getItem('LOCATION_LIST'));

    locations.push({
        date: new Date().toUTCString(),
        position: position.coords.latitude + ',' + position.coords.longitude
    });

    while (locations.length > MAX_LOCATIONS) // allow X locations only
    {
        locations.pop();
    }

    window.localStorage.setItem('LOCATION_LIST', JSON.stringify(locations));
}

function onErrorLocation(error) {
    console.log('Location failure: ' + error);
    alert('Location not found. Please try again.');
}

function captureImage() {
    navigator.camera.getPicture(onSuccessCamera, onErrorCamera, {
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA
    });
}

function getPhoto() {
    // Retrieve image file location from specified source
    navigator.camera.getPicture(attachPhoto, onFail, {
        quality: 50,
        mediaType: Camera.MediaType.PICTURE,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
    });
}

function onSuccessCamera(imageURI) {
    // image is now saved on device cache - need to copy image to gallery
	var imageName = imageURI.substr(imageURI.lastIndexOf('/')+1,imageURI.length);
	alert('Image saved! ' + imageName);
	
    // store new image url in local storage
    var images = [];
	
	// store new location in local storage
    if (window.localStorage.getItem('IMAGE_LIST') != null)
        images = JSON.parse(window.localStorage.getItem('IMAGE_LIST'));

    images.push({
        url: imageURI,
        name: imageName
    });

    while (images.length > MAX_LOCATIONS) // allow X locations only
    {
        images.pop();
    }
    window.localStorage.setItem('IMAGE_LIST', JSON.stringify(images));
}

function onErrorCamera(message) {
    console.log('Image failure: ' + message);
}

function attachPhoto(imageURI) {
    alert('Photo attached: ' + imageURI);
}

function onFail(message) {

}

function selectImage(image) {
	$("#popupMenu2").popup("close");
	
	claim.image = image;
	claim.save();
	
	$("#accident_image").text(image); // make hidden
    var largeImage = document.getElementById('largeImage');
	
	if(claim.image != "")
	{	
		largeImage.style.display = 'block';
		largeImage.src = claim.image; // put image here
	}
}

function selectLocation(location) {
	claim.location = location;
	claim.save();
	$("#accident_location").text(location);
    $("#popupMenu").popup("close");
}

function goToClaimHome() {
    var dirPath = dirname(location.href);
    fullPath = dirPath + "/claimhome.html";
    window.location = fullPath;
}

function goToPage(page) {
    var dirPath = dirname(location.href);
    fullPath = dirPath + "/" + page;
    window.location = fullPath;
}

function submitClaim() {
	// save page 5
	claim.name = $('#accident_name').val();
    claim.policy = $('#accident_policy').val();
    claim.email = $('#accident_email').val();
    claim.mobile = $('#accident_mobile').val();
    claim.tele = $('#accident_tele').val();
    claim.car = $('#accident_car').val();
	claim.type = "PREV"; // claim is now previous claim as submitted
	claim.submitdate = get_date_string();
	claim.save();
	
	var dirPath = dirname(location.href);
    fullPath = dirPath + '/claimhome.html';
    window.location = fullPath;
}

function draftClaimsLoad(id) {
	store.put('CURRENT_CLAIM_ID', id);
    open("newclaim_page1.html", "_self", "", true);
}

function prevClaimsLoad(id) {
	store.put('CURRENT_CLAIM_ID', id);
    open("prevclaim_page1.html", "_self", "", true);
}

function goToPageFrom(page, from) {
    if (from == 'newclaim_page1') {
        // save page 1
        claim.date = $('#accident_date').val();
        claim.peoplecount = $('#accident_peoplecount').val();
        claim.names = $('#accident_names').val();
        claim.speed = $('#accident_speed').val();
    } else if (from == 'newclaim_page2') {
        // save page 2
        claim.weather = $('#accident_weather').val();
        claim.visibility = $('#accident_visibility').val();
        claim.happened = $('#accident_happened').val();
        claim.details = $('#accident_details').val();
        claim.witnesses = $('#accident_witnesses').val();
    } else if (from == 'newclaim_page3') {
        // save page 3
        claim.damagedone = $('#accident_damagedone').val();
        claim.damage = $('#accident_damage').val();
    } else if (from == 'newclaim_page4') {
        // save page 4
        claim.othername = $('#accident_othername').val();
        claim.othermobile = $('#accident_othermobile').val();
        claim.othertele = $('#accident_othertele').val();
        claim.otherinsure = $('#accident_otherinsure').val();
        claim.othercar = $('#accident_othercar').val();
    } else if (from == 'newclaim_page5') {
        // save page 5
        claim.name = $('#accident_name').val();
        claim.policy = $('#accident_policy').val();
        claim.email = $('#accident_email').val();
        claim.mobile = $('#accident_mobile').val();
        claim.tele = $('#accident_tele').val();
        claim.car = $('#accident_car').val();
    }

	if (page == 'claimhome') {
		//TODO save claim as DRAFT if anything was done to claim object, otherwise don't save as draft
		if(claim.dirty == true && claim.id == -1)
			claim.type = "DRAFT";
	}

	claim.save();
	
    var dirPath = dirname(location.href);
    fullPath = dirPath + "/" + page + '.html';
    window.location = fullPath;
}

function setDraftPrevious() {
	var previousClaims = 0;
	var drafts = 0;
	
	for (var key in localStorage){
		// go through localStorage keys
		if(key.indexOf('CLAIM_') != -1){ // if item is a claim
			var aclaim = JSON.parse(store.get(key));
			if(aclaim.type == 'DRAFT')
				drafts++;
			else if(aclaim.type == 'PREV')
				previousClaims++;
		}
	}
	
	$('#btnPrevious').val("Previous Claims ("+previousClaims+")");
	$('#btnPrevious').button('refresh');
	
	
	$('#btnDraft').val("Draft Claims ("+drafts+")");
	$('#btnDraft').button('refresh');
}

function goToPage(page, policyNumber) {
    var dirPath = dirname(location.href);
    fullPath = dirPath + "/" + page;
    window.location = fullPath;
}

function dirname(path) {
    return path.replace(/\\/g, '/').replace(/\/[^\/]*$/, '');
}

function populate(page) {
    claim = new Claim();
    var id = store.get('CURRENT_CLAIM_ID');
	claim.retrieve(id);
    if (page == 'newclaim_page1') {
        // load page 1
        $('#accident_date').val(claim.date);
        $('#accident_peoplecount').val(claim.peoplecount);
        $('#accident_peoplecount').selectmenu('refresh');
        $('#accident_names').val(claim.names);
        $('#accident_speed').val(claim.speed);
		$("#accident_location").text(claim.location);
		$("#accident_image").text(claim.image);
		var largeImage = document.getElementById('largeImage');
		if(claim.image != "")
		{	
			largeImage.style.display = 'block';
			largeImage.src = claim.image; // put image here
		}
	
    } else if (page == 'newclaim_page2') {
        // load page 2
        $('#accident_weather').val(claim.weather);
        $('#accident_weather').selectmenu('refresh');
        $('#accident_visibility').val(claim.visibility);
        $('#accident_visibility').selectmenu('refresh');
        $('#accident_happened').val(claim.happened);
        $('#accident_happened').selectmenu('refresh');
        $('#accident_details').val(claim.details);
		$('#accident_details').textinput('refresh');	
        $('#accident_witnesses').val(claim.witnesses);
		$('#accident_witnesses').textinput('refresh');	
    } else if (page == 'newclaim_page3') {
        // load page 3
        $('#accident_damagedone').val(claim.damagedone);
		$('#accident_damagedone').slider('refresh');
        $('#accident_damage').val(claim.damage);
		$('#accident_damage').textinput('refresh');	
    } else if (page == 'newclaim_page4') {
        // load page 4
        $('#accident_othername').val(claim.othername);
        $('#accident_othermobile').val(claim.othermobile);
        $('#accident_othertele').val(claim.othertele);
        $('#accident_otherinsure').val(claim.otherinsure);
        $('#accident_othercar').val(claim.othercar);
    } else if (page == 'newclaim_page5') {
        // load page 5
        $('#accident_name').val(claim.name);
        $('#accident_policy').val(claim.policy);
        $('#accident_email').val(claim.email);
        $('#accident_mobile').val(claim.mobile);
        $('#accident_tele').val(claim.tele);
        $('#accident_car').val(claim.car);
    }
}

function get_date_string(){
		var date = new Date();
		//zero-pad a single zero if needed
		var zp = function (val){
			return (val <= 9 ? '0' + val : '' + val);
		}

		//zero-pad up to two zeroes if needed
		var zp2 = function(val){
			return val <= 99? (val <=9? '00' + val : '0' + val) : ('' + val ) ;
		}

		var d = date.getDate();
		var m = date.getMonth() + 1;
		var y = date.getFullYear();
		var h = date.getHours();
		var min = date.getMinutes();
		var s = date.getSeconds();
		var ms = date.getMilliseconds();
		return '' + y + '-' + zp(m) + '-' + zp(d);
}
